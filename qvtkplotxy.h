#ifndef QVTKPLOTXY_H
#define QVTKPLOTXY_H

#include <QVTKWidget.h>
#include <QVTKWidget2.h>
#include <vtkSmartPointer.h>
#include <vtkContextView.h>
#include <vtkAbstractContextItem.h>
#include <vtkChartXY.h>
#include <vtkTable.h>
class qvtkPlotXYCurveData
{
public:
    qvtkPlotXYCurveData(QVector<double> X, QVector<double> Y,QString xName="",QString name="",int lineType=1,QColor color=Qt::green,double lineWidth=1.);
    vtkSmartPointer<vtkTable> myVtkTable;
    QString myName;
    vtkPlot *myLine;
    int myLineType;
    QColor myColor;
    double myLineWidth;
    int XtableIndex;
    int YtableIndex;
};

class qvtkPlotXY : public QVTKWidget
{
    Q_OBJECT
public:
    explicit qvtkPlotXY(QWidget *parent = 0);
    ~qvtkPlotXY();
    /// Description:
    /// Add child items to myVtkContextView. Increments reference count of item.
    /// \return the index of the child item.
    unsigned int SetChart(vtkChartXY *chart);
    void addCurve(QVector<double> X,QVector<double> Y,QString xName="",QString name="",int lineType=1,QColor color=Qt::green,double lineWidth=1.);
    void setCurvesToPlot();
    void clearPlots();
signals:
    
public slots:
protected:
    vtkSmartPointer<vtkContextView> myVtkContextView;
    void setChartOptions(vtkSmartPointer<vtkChartXY> chart);

private:
    void addExemplePlot(double omega=1.);
    void setExampleCurves(double omega);
    QList<qvtkPlotXYCurveData> myCurvesData;
    QList<QColor> myPalette;
};

#endif // QVTKPLOTXY_H
