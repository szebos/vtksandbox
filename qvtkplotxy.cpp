#define VTK_CREATE(type, name) \
   vtkSmartPointer<type> name = vtkSmartPointer<type>::New()
#include "qvtkplotxy.h"
#include "vtkAxis.h"
#include "vtkTextProperty.h"
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkPlot.h>
#include <vtkFloatArray.h>
#include <vtkContextScene.h>
#include <vtkPen.h>

qvtkPlotXYCurveData::qvtkPlotXYCurveData(QVector<double> X, QVector<double> Y,QString xName, QString name, int lineType, QColor color, double lineWidth){
    myName = name;
    myLineType=lineType;
    myColor = color;
    myLineWidth = lineWidth;
    myVtkTable = vtkSmartPointer<vtkTable>::New();

    VTK_CREATE(vtkFloatArray, arrX);
    arrX->SetName(xName.toUtf8());
    myVtkTable->AddColumn(arrX);
    XtableIndex = 0;

    VTK_CREATE(vtkFloatArray, arrY );
    arrY->SetName(name.toUtf8());
    myVtkTable->AddColumn(arrY);
    YtableIndex = 1;

    if(X.size() != Y.size()){
        qDebug("Error in size.");
        return;
    }
    myVtkTable->SetNumberOfRows(X.size());
    for(int i =0 ; i<X.size();i++){
        myVtkTable->SetValue(i, XtableIndex, X[i]);
        myVtkTable->SetValue(i, YtableIndex, Y[i]);
    }
}

qvtkPlotXY::qvtkPlotXY(QWidget *parent) :
    QVTKWidget(parent)
{
// This contains a chart object
    myVtkContextView = vtkSmartPointer<vtkContextView>::New();
    myVtkContextView->SetInteractor(GetInteractor());
    myVtkContextView->GetRenderWindow()->SetMultiSamples(4);
    SetRenderWindow(myVtkContextView->GetRenderWindow());
    myVtkContextView->GetRenderer()->SetBackground(1.0, 1.0, 1.0);



    myPalette<<Qt::green<<Qt::red<<Qt::blue;

    addExemplePlot(1.);

//    // Create a table with some points in it...
//    VTK_CREATE(vtkTable, table);
//    VTK_CREATE(vtkFloatArray, arrX);
//    arrX->SetName("X Axis");
//    table->AddColumn(arrX);
//    VTK_CREATE(vtkFloatArray, arrC);
//    arrC->SetName("Cosine");
//    table->AddColumn(arrC);
//    VTK_CREATE(vtkFloatArray, arrS);
//    arrS->SetName("Sine");
//    table->AddColumn(arrS);

//    71   // Make a timer object - need to get some frame rates/render times
//    72   VTK_CREATE(vtkTimerLog, timer);
//    73
//    74   // Test charting with a few more points...
//    75   int numPoints = 29;
//    76   float inc = 7.0 / (numPoints-1);
//    77   table->SetNumberOfRows(numPoints);
//    78   for (int i = 0; i < numPoints; ++i)
//    79     {
//    80     table->SetValue(i, 0, i * inc);
//    table->SetValue(i, 1, cos(i * inc) + 0.0);
//    table->SetValue(i, 2, sin(i * inc) + 0.0);
//    }

}

qvtkPlotXY::~qvtkPlotXY(){
//    delete myVtkContextView;//Useless with smart pointer
}

unsigned int qvtkPlotXY::SetChart(vtkChartXY* chart){
    myVtkContextView->GetScene()->ClearItems();
    myVtkContextView->GetScene()->AddItem(chart);
}

void qvtkPlotXY::addCurve(QVector<double> X, QVector<double> Y,QString xName, QString name, int lineType, QColor color, double lineWidth){
    myCurvesData<<qvtkPlotXYCurveData(X,Y,xName,name,lineType,color,lineWidth);
}


void qvtkPlotXY::setExampleCurves(double omega){

    int numPoints = 69;
    float inc = 7.5 / (numPoints-1);
    QVector<double> X,Y1,Y2;
    for (int i = 0; i < numPoints; ++i)
    {
        X<< i * inc;
        Y1<< cos(i * inc * omega);
        Y2<< sin(i * inc * omega);
    }
    myCurvesData<<qvtkPlotXYCurveData(X,Y1,"X Axis","Cosine");
    myCurvesData<<qvtkPlotXYCurveData(X,Y2,"X Axis","Sine",2,Qt::red,3.);
    setCurvesToPlot();
}

void qvtkPlotXY::setChartOptions(vtkSmartPointer<vtkChartXY> chart){
//    chart->GetAxis(vtkAxis::BOTTOM)->GetTitleProperties()->SetFontFamilyToArial();
//    chart->GetAxis(vtkAxis::BOTTOM)->GetTitleProperties()->SetColor(1.,1.,1.);
    chart->GetAxis(vtkAxis::BOTTOM)->GetTitleProperties()->ShadowOn();
//    chart->GetAxis(vtkAxis::BOTTOM)->GetTitleProperties()->SetShadowOffset(100,100);
    chart->GetAxis(vtkAxis::BOTTOM)->GetTitleProperties()->SetFontSize(18);
}

void qvtkPlotXY::setCurvesToPlot(){
    myVtkContextView->GetScene()->ClearItems();
    vtkSmartPointer<vtkChartXY> chart =
      vtkSmartPointer<vtkChartXY>::New();
    for(int i = 0 ; i < myCurvesData.size() ; i++){
        vtkPlot *line = chart->AddPlot(vtkChart::LINE);
      #if VTK_MAJOR_VERSION <= 5
        line->SetInput(myCurvesData[i].myVtkTable, myCurvesData[i].XtableIndex, myCurvesData[i].YtableIndex);
      #else
        line->SetInputData(myCurvesData[i].myVtkTable, myCurvesData[i].XtableIndex, myCurvesData[i].YtableIndex);
      #endif
        line->SetColor(myCurvesData[i].myColor.red(),
                       myCurvesData[i].myColor.green(),
                       myCurvesData[i].myColor.blue(),
                       myCurvesData[i].myColor.alpha()
                       );
        line->SetWidth(myCurvesData[i].myLineWidth);
        line->GetPen()->SetLineType(myCurvesData[i].myLineType);//For dotted line, can be from 2 to 5 for different dot patterns
    }
    myVtkContextView->GetScene()->AddItem(chart);
    setChartOptions(chart);
}

void qvtkPlotXY::addExemplePlot(double omega){

    setExampleCurves(omega);
    setCurvesToPlot();
}
