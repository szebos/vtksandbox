#ifndef WIDGETXYPLOT_H
#define WIDGETXYPLOT_H

#include <QVTKWidget.h>

class WidgetXYPlot : public QVTKWidget
{
    Q_OBJECT
    
public:
    WidgetXYPlot(QWidget *parent = 0);
    ~WidgetXYPlot();
};

#endif // WIDGETXYPLOT_H
