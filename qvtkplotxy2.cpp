#include "qvtkplotxy2.h"

#include "vtkGenericOpenGLRenderWindow.h"
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkChartXY.h>
#include <vtkTable.h>
#include <vtkPlot.h>
#include <vtkFloatArray.h>
#include <vtkContextScene.h>
#include <vtkPen.h>
#include <QVTKInteractor.h>
#define VTK_CREATE(type, name) \
   vtkSmartPointer<type> name = vtkSmartPointer<type>::New()

qvtkplotXY2::qvtkplotXY2(QWidget *parent) :
    QVTKWidget2(parent)
{
    myVtkContextView = vtkSmartPointer<vtkContextView>::New();
    VTK_CREATE(vtkGenericOpenGLRenderWindow,renderWindow);
    myVtkContextView->SetRenderWindow(renderWindow);
    myVtkContextView->SetInteractor(GetInteractor());
    myVtkContextView->GetRenderWindow()->SetMultiSamples(4);
    SetRenderWindow(renderWindow);
//    SetRenderWindow(myVtkContextView->GetRenderWindow());
///*
    VTK_CREATE(vtkTable, table);

    VTK_CREATE(vtkFloatArray, arrX);
    arrX->SetName("X Axis");
    table->AddColumn(arrX);

    VTK_CREATE(vtkFloatArray, arrC );
    arrC->SetName("Cosine");
    table->AddColumn(arrC);

    VTK_CREATE(vtkFloatArray, arrS );
    arrS->SetName("Sine");
    table->AddColumn(arrS);

    // Fill in the table with some example values
    int numPoints = 69;
    float inc = 7.5 / (numPoints-1);
    table->SetNumberOfRows(numPoints);
    for (int i = 0; i < numPoints; ++i)
    {
      table->SetValue(i, 0, i * inc);
      table->SetValue(i, 1, cos(i * inc));
      table->SetValue(i, 2, sin(i * inc));
    }

    myVtkContextView->GetRenderer()->SetBackground(1.0, 1.0, 1.0);

    // Add multiple line plots, setting the colors etc
    vtkSmartPointer<vtkChartXY> chart =
      vtkSmartPointer<vtkChartXY>::New();
    myVtkContextView->GetScene()->AddItem(chart);
    vtkPlot *line = chart->AddPlot(vtkChart::LINE);
  #if VTK_MAJOR_VERSION <= 5
    line->SetInput(table, 0, 1);
  #else
    line->SetInputData(table, 0, 1);
  #endif
    line->SetColor(0, 255, 0, 255);
    line->SetWidth(1.0);
    line = chart->AddPlot(vtkChart::LINE);
  #if VTK_MAJOR_VERSION <= 5
    line->SetInput(table, 0, 2);
  #else
    line->SetInputData(table, 0, 2);
  #endif
    line->SetColor(255, 0, 0, 255);
    line->SetWidth(5.0);
    line->GetPen()->SetLineType(2);//For dotted line, can be from 2 to 5 for different dot patterns
//*/
}
