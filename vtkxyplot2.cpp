/*=========================================================================

      Program:   Full Turbo Modeling & Simulation (FTMS)
      Module:    Turbo Geom

      Copyright (c) :
    NEWe Ingenieria S.L
    Calle Isabel la catolica, 12
    11004
      All rights reserved.
      See Copyright.txt or http://www.newe.es/Copyright.htm for details.
      the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
      PURPOSE.  See the above copyright notice for more information.

      Author  : Sébastien Raymond
      Contact : sebastien.newe@gmail.com
=========================================================================*/
#include "vtkxyplot2.h"


#include <vtkSmartPointer.h>
#include <vtkSphereSource.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkGenericOpenGLRenderWindow.h>
#include <vtkChartXY.h>
#include <vtkTable.h>
#include <vtkPlot.h>
#include <vtkFloatArray.h>
#include <vtkContextView.h>
#include <vtkContextScene.h>
#include <vtkPen.h>
#include <vtkXYPlotActor.h>
#include <vtkPolyData.h>
#include <vtkCellArray.h>

vtkXYPlot2::vtkXYPlot2(QWidget *parent) :
    QVTKWidget2(parent)
{
    // Setup window
//    vtkSmartPointer<vtkRenderWindow> renderWindow =
//        vtkSmartPointer<vtkRenderWindow>::New();
    vtkSmartPointer<vtkGenericOpenGLRenderWindow> renderWindow =
            vtkSmartPointer<vtkGenericOpenGLRenderWindow>::New();

    // Setup renderer
    vtkSmartPointer<vtkRenderer> renderer =
        vtkSmartPointer<vtkRenderer>::New();
    renderer->SetBackground(1.0, 1.0, 1.0);
    renderWindow->AddRenderer(renderer);
    // Setup plot
    vtkSmartPointer<vtkXYPlotActor> XYPlotActor =
            vtkSmartPointer<vtkXYPlotActor>::New();
    vtkSmartPointer<vtkPolyData> data1 =
            vtkSmartPointer<vtkPolyData>::New();
//    data1->SetLines();


//    XYPlotActor->SetPlotLines();
//    renderer->AddActor2D(XYPlotActor);

    SetRenderWindow(renderWindow);
//    myWidget = new QWidget(parent);
//    // Create a table with some points in it
//      vtkSmartPointer<vtkTable> table =
//        vtkSmartPointer<vtkTable>::New();

//      vtkSmartPointer<vtkFloatArray> arrX =
//        vtkSmartPointer<vtkFloatArray>::New();
//      arrX->SetName("X Axis");
//      table->AddColumn(arrX);

//      vtkSmartPointer<vtkFloatArray> arrC =
//        vtkSmartPointer<vtkFloatArray>::New();
//      arrC->SetName("Cosine");
//      table->AddColumn(arrC);

//      vtkSmartPointer<vtkFloatArray> arrS =
//        vtkSmartPointer<vtkFloatArray>::New();
//      arrS->SetName("Sine");
//      table->AddColumn(arrS);

//      // Fill in the table with some example values
//      int numPoints = 69;
//      float inc = 7.5 / (numPoints-1);
//      table->SetNumberOfRows(numPoints);
//      for (int i = 0; i < numPoints; ++i)
//      {
//        table->SetValue(i, 0, i * inc);
//        table->SetValue(i, 1, cos(i * inc));
//        table->SetValue(i, 2, sin(i * inc));
//      }
//      GetRenderer()->SetBackground(1.0, 1.0, 1.0);

//        // Add multiple line plots, setting the colors etc
//        vtkSmartPointer<vtkChartXY> chart =
//          vtkSmartPointer<vtkChartXY>::New();
//        GetScene()->AddItem(chart);
//        vtkPlot *line = chart->AddPlot(vtkChart::LINE);
//      #if VTK_MAJOR_VERSION <= 5
//        line->SetInput(table, 0, 1);
//      #else
//        line->SetInputData(table, 0, 1);
//      #endif
//        line->SetColor(0, 255, 0, 255);
//        line->SetWidth(1.0);
//        line = chart->AddPlot(vtkChart::LINE);
//      #if VTK_MAJOR_VERSION <= 5
//        line->SetInput(table, 0, 2);
//      #else
//        line->SetInputData(table, 0, 2);
//      #endif
//        line->SetColor(255, 0, 0, 255);
//        line->SetWidth(5.0);
//        line->GetPen()->SetLineType(2);//For dotted line, can be from 2 to 5 for different dot patterns

//        //view->GetRenderWindow()->SetMultiSamples(0);

//        // Start interactor
//        GetInteractor()->Initialize();
//        GetInteractor()->Start();
}
