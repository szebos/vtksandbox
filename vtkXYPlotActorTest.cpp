#include <vtkXYPlotActorTest.h>

#include <vtkSmartPointer.h>
#include <vtkXYPlotActor.h>
#include <vtkCellArray.h>
#include <vtkPolyData.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <QVTKWidget.h>

#define VTK_CREATE(type, name) \
   vtkSmartPointer<type> name = vtkSmartPointer<type>::New()

void vtkXYPlotActorTest(){
    VTK_CREATE(vtkXYPlotActor,xyplot );
    VTK_CREATE(vtkPolyData,pointSet);
    VTK_CREATE(vtkCellArray,points);

    pointSet->SetLines(points);
    xyplot->AddDataSetInput(pointSet);




    VTK_CREATE(vtkRenderWindow,renWin);
    VTK_CREATE(vtkRenderer,ren);
    ren->AddActor2D(xyplot);
    renWin->AddRenderer(ren);
    QVTKWidget *w = new QVTKWidget;
    w->SetRenderWindow(renWin);
    w->show();
}
