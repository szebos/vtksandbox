#ifndef QVTKPLOTXY2_H
#define QVTKPLOTXY2_H

#include <QVTKWidget2.h>
#include <vtkSmartPointer.h>
#include <vtkContextView.h>

class qvtkplotXY2 : public QVTKWidget2
{
    Q_OBJECT
public:
    explicit qvtkplotXY2(QWidget *parent = 0);
    
signals:
    
public slots:
protected:
    vtkSmartPointer<vtkContextView> myVtkContextView;
};

#endif // QVTKPLOTXY2_H
