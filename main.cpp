#define vtkRenderingCore_AUTOINIT 4(vtkInteractionStyle,vtkRenderingFreeType,vtkRenderingFreeTypeOpenGL,vtkRenderingOpenGL)
#define vtkRenderingVolume_AUTOINIT 1(vtkRenderingVolumeOpenGL)
#include "vtkxyplot2.h"
#include "qvtkplotxy.h"
#include "qvtkplotxy2.h"
#include <vtkXYPlotActorTest.h>
#include <testMultiBlock.h>
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
//    qvtkPlotXY w;
//    w.show();
//    qvtkplotXY2 w2;
//    w2.show();
//    vtkXYPlotActorTest();
    testMB();
    return a.exec();
}

