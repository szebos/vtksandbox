/*=========================================================================

      Program:   Full Turbo Modeling & Simulation (FTMS)
      Module:    Turbo Geom

      Copyright (c) :
    NEWe Ingenieria S.L
    Calle Isabel la catolica, 12
    11004
      All rights reserved.
      See Copyright.txt or http://www.newe.es/Copyright.htm for details.
      the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
      PURPOSE.  See the above copyright notice for more information.

      Author  : Sébastien Raymond
      Contact : sebastien.newe@gmail.com
=========================================================================*/
#ifndef VTKXYPLOT2_H
#define VTKXYPLOT2_H
#include <QVTKWidget2.h>
#include <QVTKWidget.h>
class vtkXYPlot2 : public QVTKWidget2
{
    Q_OBJECT
public:
    explicit vtkXYPlot2(QWidget *parent = 0);
//    QWidget * GetWidget();
signals:
    
public slots:
private :
//    QWidget *myWidget;
};

#endif // VTKXYPLOT2_H
