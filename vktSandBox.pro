#-------------------------------------------------
#
# Project created by QtCreator 2013-11-01T15:26:52
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = vktSandBox
TEMPLATE = app


SOURCES += main.cpp\
        widgetxyplot.cpp \
    vtkxyplot2.cpp \
    qvtkplotxy.cpp \
    qvtkplotxy2.cpp \
    vtkXYPlotActorTest.cpp \
    testMultiBlock.cpp

HEADERS  += widgetxyplot.h \
    vtkxyplot2.h \
    qvtkplotxy.h \
    qvtkplotxy2.h \
    vtkXYPlotActorTest.h \
    testMultiBlock.h

include(../vtkLinkage.pri)
